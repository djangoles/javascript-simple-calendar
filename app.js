const daysKey = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
const monthsKey = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
const grid = document.querySelector('.grid')
const calendarDisplay = document.querySelector('.calendar-display')

const inputYearSelect = document.querySelector('.input-year-select')
const inputMonthSelect = document.querySelector('.input-month-select')

const yearMonthForm = document.querySelector('.year-month-form')
const submitBTN = document.querySelector('.submit-btn')
const monthYearHeaderText = document.querySelector('.month-year-header-text')

const createYearArray = () => {
    let years = []
    let counter = 2018
    while(counter <  2051) {
        years.push(counter)
        counter++
    }
    return years
}

const loadYearMonthSelect = (arrYears, arrMonths) => {
    arrYears.forEach(element => {
        let optionMonthSelect = document.createElement('option')
        optionMonthSelect.value = element
        optionMonthSelect.textContent = element
        inputYearSelect.appendChild(optionMonthSelect)
    });
    arrMonths.forEach((month, i) => {
        let optionMonthSelect = document.createElement('option')
        optionMonthSelect.value = i
        optionMonthSelect.textContent = month
        inputMonthSelect.appendChild(optionMonthSelect)
    });
}

const getFirstDayOfMonth = (year, month, arrOfDays) => {
    let dayInt = (new Date(year, month)).getDay();
    return arrOfDays[dayInt]
}

const daysInMonth = (month, year) => {
    let numDaysInMonth = 32 - new Date(year, month, 32).getDate();
    return  numDaysInMonth
}

const getMonthYear = (e) => {
    e.preventDefault()
    let year = parseInt(inputYearSelect.value)
    let month = parseInt(inputMonthSelect.value)
    const firstDayMon = getFirstDayOfMonth(year, month, daysKey)
    const numDaysMon = daysInMonth(month, year)
    updateUIHeader(monthsKey[month], year)
    loadCalendarBoard(7,7, firstDayMon, numDaysMon, year, month)
}

const loadCalendarBoard = (row, col, firstDay, numDaysMonth, yr, mon) => {
    calendarDisplay.innerHTML = ''
    calendarDisplay.dataset.month = mon
    calendarDisplay.dataset.year = yr
    let prevMonth
    let prevYear
    mon === 0 ?  prevMonth = 11 : prevMonth = mon - 1
    mon === 0 ?  prevYear = yr -1 : prevYear = yr
    const prevMonthNumDays = daysInMonth(prevMonth, yr)

    let counter = 0
    let nextMonthCounter = 0
    let prevMonthCounter = prevMonthNumDays - daysKey.indexOf(firstDay)

    for (var i = 0; i < row; i++) {
        for (var j = 0; j < col; j++) {
            let button = document.createElement('button')
            button.classList.add('btn')  

            // ADDS DAYS OF WEEK ====================
            if(i === 0) {
                button.textContent =  daysKey[j]       
            }
            // CHECKS FIRST ROW FOR AN OFFSET ============================== 
            if(i === 1 )  {
                if(j < daysKey.indexOf(firstDay)) {
                    // FOR SPACES BEFORE MONTH BEGIN
                    prevMonthCounter++
                    button.textContent =  prevMonthCounter 
                } else {
                    counter++ 
                    button.classList.add('numbers')  
                    button.dataset.id = counter
                    button.textContent =  counter
                }
            }
            // EVERYTHING AFTER FIRST ROW OF DATES =========================
            if(i > 1) {
                // FOR SPACES AFTER MONTH END
                if(counter >= numDaysMonth) {
                    counter++
                    nextMonthCounter++
                    button.textContent = nextMonthCounter 
                } else {
                    counter++
                    button.classList.add('numbers') 
                    button.dataset.id = counter 
                    button.textContent =  counter 
                }    
            }  
            calendarDisplay.appendChild(button)  
        }
    } 
}

const updateUIHeader = (month, year) => {
    monthYearHeaderText.textContent = `${month} ${year}`
}

const yearArr = createYearArray();
loadYearMonthSelect(yearArr, monthsKey)
yearMonthForm.addEventListener('submit', getMonthYear)

